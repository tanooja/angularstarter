import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {

  @Input() testValue: number;
  @Input() user;

  @Input() myname = '';


  // save(form) {
  //   console.log(form.value);
  // }

}
