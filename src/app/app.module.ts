import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
 // import { CounterComponent } from './counter/counter.component';
import { AppComponent } from './app.component';
import { TodosComponent } from './todos/todos.component';
import { TodoComponent } from './todo/todo.component';
import { TodoService } from './todo.service';

import { FormsModule } from '@angular/forms';
import { TodoFormComponent } from './todoform/todoform.component';
import { TodoListComponent } from './todolist/todolist.component';
import { TestComponent } from './test/test.component';
import { SignupFormComponent } from './signupform/signupform.component';





@NgModule({
  declarations: [
    AppComponent,
    TodosComponent,
    TodoComponent,
    TodoFormComponent,
    TodoListComponent,
    TestComponent,
    SignupFormComponent,


  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule  {

}
