import { Component, OnInit } from '@angular/core';
import { TodoService, ToDoItem } from '../todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos: any[];
  selectedToDo: ToDoItem;
  isEdit= false;
  constructor(private todoService: TodoService) {}
  ngOnInit() {
    this.todos = this.todoService.getTodos();
  }
  onEditClick(id) {
    this.isEdit =  true;
    const todo  = this.todos.filter(it => id ===  it.id)[0];
    this.selectedToDo = {
      id: todo.id,
      label: todo.label,
      complete: todo.complete
    };
  }
  addTodo({label}) {
    this.todos = [{label, id: this.todos.length + 1}, ...this.todos];
  }
  completeTodo({todo}) {
    this.todos = this.todos.map(
      item => item.id === todo.id ? Object.assign({}, item, {complete: true}) : item
    );
  }
  removeTodo({todo}) {
    this.todos = this.todos.filter(({id}) => id !== todo.id);
  }
  update({label}) {
    this.todos = [{label, id: this.todos.length + 1}, ...this.todos];
  }

}
