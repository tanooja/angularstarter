import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ToDoItem } from '../todo.service';

@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoFormComponent {
  label: string;
  @Input() isEdit: boolean;
  @Input() selectedToDo: any;
  @Output() onAdd = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() onEditClick= new EventEmitter();
  submit() {
    if (!this.label) { return;
    }
    this.onAdd.emit({label: this.label});
    this.label = '';
  }
}
