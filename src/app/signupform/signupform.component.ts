import { Component } from '@angular/core';
import { User } from '../signupinterface';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signupform.component.html',
})
export class SignupFormComponent {
  user: User = {
    name: '',
    account: {
      email: '',
      confirm: ''
    }
  };
  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    console.log(value, valid);
  }
}
